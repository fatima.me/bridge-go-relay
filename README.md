# GO RELAY

A relay to send events to Chromia from Go

# Contents

/go: Go package used to send requests

/server: Relay server that must be running so that Go can send requests to it