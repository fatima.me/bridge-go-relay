import {Config} from "./src/config/config";

export const config: Config = {
    nodes: {
        // nftEvents: {
        //     "url": "http://localhost:7740",
        //     "privKey": "B0F7AA20A740F668005E4254494EBC3283C37CB57FF3B05ECF1D61FCB9175662",
        //     "chainId": 0
        // },
        ownership: {
            "url": "http://localhost:7740",
            "privKey": "9E3406CDA1E0C8BDE74855DCA10015DABEF3C3E4CE549F194AC0E44CDBF121BF",
            "chainId": 0
        }
    },
    debug: true
}