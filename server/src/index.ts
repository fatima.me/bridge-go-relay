import {runApi} from "./api";
import {config} from "../config";
import {RelayManager} from "./services/relay-manager";


const main = async () => {
    await RelayManager.initialize(config.nodes)

    runApi()
}

main().then()