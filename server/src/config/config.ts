export enum NodeId {
    NFT_EVENTS = "nftEvents",
    OWNERSHIP = "ownership"
}

export type Config = {
    nodes: Nodes
    debug?: boolean
}

export type Nodes = {
    [k in NodeId]?: NodeConfig
}

export interface NodeConfig {
    url: string,
    privKey: string
    chainId: number
}