import dotenv from "dotenv"
import express from 'express';
import morganBody from 'morgan-body';
import bodyParser from "body-parser";

import {config} from "../config";
import {PostchainRouter} from "./router/router";

dotenv.config()

export const runApi = () => {
    const app = express()
    const port = process.env.PORT ? process.env.PORT : 3000

    app.use(bodyParser.json({limit: "50mb"}))
    if (config.debug) {
        morganBody(app, {maxBodyLength: 1000})
    }
    app.use('/', PostchainRouter())

    app.listen(port, () => {
        console.log(`App running on port ${port}.`)
    });
}