// @ts-ignore
import {Response, Send} from 'express-serve-static-core';

export interface TypedRequest<T> extends Express.Request {
    body: T
    query: T
}

export interface TypedResponse<T = any> extends Response {
    status(code: number): this
    json: Send<ResponseObj<T>, this>
}

export interface ResponseObj<T> {
    result: T | null,
    error: string
}

export function successRes<T>(res?: T): ResponseObj<T> {
    return {
        result: res ? res : null,
        error: ""
    }
}

export function failRes<T>(error: string, res?: T): ResponseObj<T> {
    return {
        result: res ? res : null,
        error: error
    }
}