// Maps error message to an error type. Full reason must match also to ensure the error is the one expected.
import {failRes, TypedResponse} from "./typed-express";

const ERROR_TYPE: ErrorTypes = {
    "Transaction was rejected": {
        code: 200,
        fullReason: "Failed to save tx to database",
        message: "Transaction was rejected: Failed to save tx to database"
    }
}

const unrecoverableError: ErrorType = {
    code: 512,
    fullReason: "Unrecoverable Error",
    message: "Unrecoverable Error",
}


interface ErrorTypes {
    [message: string]: ErrorType
}

interface ErrorType {
    code: number,
    fullReason: string
    message: string
}

export function getErrorType(err: any): ErrorType {
    const errType = ERROR_TYPE[err.message]

    if (!errType || (err.fullReason != errType.fullReason)) {
        return {... unrecoverableError, message: `${unrecoverableError.message}: ${
            err.shortReason 
                ? err.shortReason 
                : (err.fullReason
                    ? err.fullReason
                    : err.message)
        }`}
    }

    return errType
}

export function handleError(err: any, res: TypedResponse) {
    console.log('> handleError =>', err);
    const errType = getErrorType(err)
    res.status(errType.code).json(failRes(errType.message))
}