export interface OperationRequest {
    nodeId: string
    operation: string
    args: Args
}

type Args = (string | number | Args)[]

export interface QueryRequest {
    nodeId: string
    query: string
    params: object
}