import {Router} from 'express';

import {handleError} from "./error";
import { config } from '../../config';
import {RelayManager} from "../services/relay-manager";
import {OperationRequest, QueryRequest} from "./models/requests";
import {successRes, TypedRequest, TypedResponse} from "./typed-express";

export const PostchainRouter = () => {
    const router = Router()

    router.post('/', async (req: TypedRequest<OperationRequest>, res: TypedResponse) => {
        try {
            if(!Boolean(config.nodes[req.body.nodeId])) {
                console.log(`Node with ID ${req.body.nodeId} is disabled`);
                res.status(200).json(successRes())
            } else {
                await RelayManager.sendOperation(req.body)
                res.status(200).json(successRes())
            }

        } catch (e) {
            handleError(e, res)
        }
    })

    router.post('/query', async (req: TypedRequest<QueryRequest>, res: TypedResponse) => {
        try {
            const r = await RelayManager.sendQuery(req.body)
            res.status(200).json(successRes(r))
        } catch (e) {
            handleError(e, res)
        }
    })

    return router
}