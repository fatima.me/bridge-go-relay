import {
    Blockchain,
    FlagsType,
    GtvSerializable,
    KeyPair,
    Operation,
    Postchain,
    SingleSignatureAuthDescriptor,
    User,
    nop,
} from "ft3-lib";
import {retryAsync} from "ts-retry";
import fetch from "node-fetch"
import * as dotenv from "dotenv"
import * as secp256k1 from "secp256k1";
import {NodeConfig} from "../config/config";

dotenv.config()

export class PostchainManager {
    private blockchain!: Blockchain;
    user!: User;

    private constructor(blockchain: Blockchain, user: User) {
        this.blockchain = blockchain
        this.user = user
    }

    static async new(config: NodeConfig): Promise<PostchainManager> {
        const res = await fetch(`${config.url}/brid/iid_${config.chainId}`, {method: "GET"})
        const blockchain = await new Postchain(config.url).blockchain(await res.text())

        const keyPair = new KeyPair(config.privKey);
        const singleSigAuthDescriptor = new SingleSignatureAuthDescriptor(
            keyPair.pubKey,
            [FlagsType.Account, FlagsType.Transfer]
        );
        const user = new User(keyPair, singleSigAuthDescriptor);

        return new PostchainManager(blockchain, user)
    }

    static operation(operation: string, ... gtvs: Array<GtvSerializable>) {
        return new Operation(operation, ... gtvs);
    }

    async treatOperation(op: Operation) {
        await retryAsync(async () => {
            await this.blockchain.transactionBuilder().add(op).add(nop()).buildAndSign(this.user).post();
        }, {
            maxTry: 3,
            delay: 1000,
        });
    }

    async call(op: Operation) {
        await this.blockchain.call(op, this.user)
    }

    async query(name: string, params: any): Promise<any> {
        await this.blockchain.query(name, params);
    }
}
