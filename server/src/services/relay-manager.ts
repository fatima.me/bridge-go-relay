import {PostchainManager} from "./postchain-manager";
import {NodeConfig, Nodes} from "../config/config";
import {OperationRequest, QueryRequest} from "../router/models/requests";

export class RelayManager {
    private static instance: RelayManager
    nodeMap: PostchainNodeMap = {}

    private constructor() {}

    static getInstance() {
        if (!this.instance) {
            throw Error("Call initialize() before calling getInstance()")
        } else {
            return this.instance
        }
    }

    static async initialize(nodes: Nodes): Promise<RelayManager> {
        const rm = new RelayManager()
        for (const nodeId in nodes) {
            await rm.addNode(nodeId, nodes[nodeId as keyof object])
        }
        this.instance = rm
        return this.instance
    }

    async addNode(nodeId: string, config: NodeConfig) {
        this.nodeMap[nodeId] = await PostchainManager.new(config)
    }

    static getNode(nodeId: string): PostchainManager {
        const instance = this.getInstance()
        if (!instance.nodeMap[nodeId]) {
            throw Error("Relay for node with id " + nodeId + " does not exist in server")
        }
        return instance.nodeMap[nodeId]
    }

    static async sendOperation(req: OperationRequest) {
        const node = this.getNode(req.nodeId)
        return await node.treatOperation(PostchainManager.operation(req.operation, ...req.args))
    }

    static async sendQuery(req: QueryRequest): Promise<object | undefined> {
        const node = this.getNode(req.nodeId)
        const res = await node.query(req.query, req.params)
        if (!res) {
            return undefined
        }
        return JSON.parse(JSON.stringify(res));
    }
}

export interface PostchainNodeMap {
    [nodeId: string]: PostchainManager
}