# Relay

An API to send requests to Chromia.

Runs on port 3000 by default.

# config.ts

This is the relay configuration file located in the root of the project. 

Each postchain node to send requests to can be configured in the config.ts file, in the nodes object. Each node must have specified:

    "url": URL to the postchain node
    "privKey": Private key of the user that will send the requests
    "chainId": Chain id of the node

If "debug" is present and true, the api will log requests and responses.

#### Requests

Operation and Query requests must indicate the nodeId, with this value the Relay finds the node endpoint, id and privKey using the
values configured in config.ts

An operation request from Go or other source looks like this, where args is the GTV arguments that will be passed
directly to the Postchain node:

``` 
OperationRequest {
    nodeId: string
    operation: string
    args: Args
}

type Args = (string | number | Args)[]
```

A query request from Go or other source looks like this, where params is the json to be passed as parameters
to the request:

``` 
QueryRequest {
    nodeId: string
    query: string
    params: object
}
```