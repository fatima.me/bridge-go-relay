package utils

import (
	"errors"
	"github.com/ethereum/go-ethereum/core/types"
	"math/big"
	"strings"
	"github.com/ethereum/go-ethereum/common"
)

type ItemType int

const (
	ItemTypeTypeNative ItemType = iota
	ItemTypeTypeERC20
	ItemTypeTypeERC721
	ItemTypeTypeERC1155
)

type TransferTopic string

var (
	// Transfer(address,address,uint256)
	TransferTopicErc721 TransferTopic = "0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef"
	// TransferSingle(address,address,address,uint256,uint256)
	TransferTopicErc155Single TransferTopic = "c3d58168c5ae7397731d063d5bbf3d657854427343f4c083240f7aacaa2d0f62"
	// TransferBatch(address,address,address,uint256[],uint256[])
	TransferTopicErc155Batch TransferTopic = "4a39dc06d4c0dbc64b70af90fd698a233a518aa5d07e595d983b8c0526c8f7fb"
)

type TransferArgs struct {
	ItemType ItemType
	From     string
	To       string
	TokenId  int64
	Amount   string
}

func TransferArgsFromLog(l *types.Log) (TransferArgs, error) {
	eventTopic := TransferTopic(strings.ToLower(l.Topics[0].String()))
	switch eventTopic {
	case TransferTopicErc721:
		if len(l.Topics) != 4 {
			return TransferArgs{}, errors.New("wrong number of topics in log, must have 4. Tx hash " + l.TxHash.Hex())
		}
		b := big.NewInt(0)
		_, ok := b.SetString(l.Topics[3].String()[2:], 16)
		if !ok {
			return TransferArgs{}, errors.New("failed to convert token id to big.Int")
		}

		// Parse the 'from' and 'To' address from the topics
		fromAddress := common.HexToAddress(l.Topics[1].Hex()).Hex()
		toAddress := common.HexToAddress(l.Topics[2].Hex()).Hex()

		return TransferArgs{
			ItemType: ItemTypeTypeERC721,
			From:     fromAddress,
			To:       toAddress,
			TokenId:  b.Int64(),
			Amount:   "1",
		}, nil
	case TransferTopicErc155Single, TransferTopicErc155Batch:
		// TODO: Add support for ERC1155s
		return TransferArgs{}, errors.New("Unsupported log with topic 0: " + string(eventTopic))
	default:
		return TransferArgs{}, errors.New("Unsupported log with topic 0: " + string(eventTopic))
	}
}
