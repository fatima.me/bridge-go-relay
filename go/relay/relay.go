package relay

import (
	"bytes"
	"encoding/json"
	"errors"
	"bitbucket.org/chromawallet/go-relay/go/gtv"
	"io"
	"net/http"
	"net/url"
	"go.uber.org/zap"
)

var (
	ErrNilResult = errors.New("null result")
)

type Endpoint string

// Op is the object expected by POST {relay}/
type Op struct {
	NodeId    string      `json:"nodeId"`
	Operation string      `json:"operation"`
	Args      interface{} `json:"args"`
}

// Qu is the object expected by POST {relay}/query
type Qu struct {
	NodeId string      `json:"nodeId"`
	Query  string      `json:"query"`
	Params interface{} `json:"params"`
}

type Relay struct {
	URL    string
	Logger *zap.Logger
}

func NewRelay(relayUrl string, logger *zap.Logger) (*Relay, error) {
	_, err := url.Parse(relayUrl)

	if logger == nil {
		logger, _ = zap.NewProduction()
	}

	return &Relay{URL: relayUrl, Logger: logger}, err
}

func (r *Relay) GenerateURL(endpoint Endpoint) (string, error) {
	return url.JoinPath(r.URL, string(endpoint))
}

func Operation(url string, nodeId string, operation string, args ...gtv.Serializable) (Result, error) {
	argsGtvs := make([]interface{}, len(args))
	for i, arg := range args {
		argsGtvs[i] = arg.Gtv()
	}
	return post(url, Op{
		NodeId:    nodeId,
		Operation: operation,
		Args:      argsGtvs,
	})
}

func Query[T interface{}](url string, nodeId string, query string, params interface{}, res T) (T, error) {
	url += "/query"
	result, err := post(url, Qu{
		NodeId: nodeId,
		Query:  query,
		Params: params,
	})
	if err != nil {
		return res, err
	}
	if result.Result == nil {
		return res, ErrNilResult
	}
	return result.Result.(T), nil
}

func post(relayUrl string, payload interface{}) (Result, error) {
	result := Result{}

	// Convert struct to JSON
	j, err := json.Marshal(&payload)
	if err != nil {
		return result, NewRelayerError(ErrClientStatusCode, err)
	}

	// Make request
	resp, err := http.Post(relayUrl, "application/json", bytes.NewReader(j))
	if err != nil {
		return result, NewRelayerError(ErrClientStatusCode, err)
	}

	// Convert response bytes to struct
	err = GetBody(resp, &result)
	if err != nil {
		return result, NewRelayerError(ErrClientStatusCode, err)
	}

	// Handle errors
	if resp.StatusCode > 299 || resp.StatusCode < 200 {
		return result, NewRelayerError(resp.StatusCode, errors.New(result.Error))
	}

	return result, nil
}

func GetBody(r *http.Response, result *Result) error {
	// Convert response bytes to struct
	if r != nil && r.Body != nil {
		defer r.Body.Close()
		b, _ := io.ReadAll(r.Body)
		err := json.Unmarshal(b, result)
		if err != nil {
			return NewRelayerError(ErrClientStatusCode, err)
		}
	}
	return nil
}
