package relay

import (
	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRelayerError_Wrap(t *testing.T) {
	err := NewRelayerError(404, errors.New("ERROR"))
	err = fmt.Errorf("Error: %w", err)

	errActual := errors.Unwrap(err)
	assert.True(t, errors.As(errActual, &RelayError{}))
	assert.Equal(t, 404, errActual.(RelayError).StatusCode)
}
