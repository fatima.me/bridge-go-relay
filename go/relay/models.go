package relay

type Result struct {
	Result interface{} `json:"result"`
	Error  string      `json:"error"`
}
