package relay

import "fmt"

const (
	ErrClientStatusCode = -1
)

type RelayError struct {
	StatusCode int
	Err        error
}

func (r RelayError) Error() string {
	return fmt.Errorf("response status %d. %w", r.StatusCode, r.Err).Error()
}

func NewRelayerError(statusCode int, err error) error {
	return RelayError{
		StatusCode: statusCode,
		Err:        fmt.Errorf("%w", err),
	}
}
