package nftevents

type EventType int8
type SaleType int8

const (
	EventTypeTransfer EventType = iota
	EventTypeSale
)

const (
	SaleTypeRegular SaleType = iota
	SaleTypeBundle
)
