package nftevents

import (
	"bitbucket.org/chromawallet/go-relay/go/gtv"
	"fmt"
	"bitbucket.org/chromawallet/go-relay/go/relay"
	"go.uber.org/zap"
)

const (
	nodeId = "nftEvents"
)

type RelayNftEvents struct {
	*relay.Relay
}

func NewRelay(relayUrl string, logger *zap.Logger) (*RelayNftEvents, error) {
	r, err := relay.NewRelay(relayUrl, logger)
	if err != nil {
		return nil, fmt.Errorf("failed to create new RelayNftEvents: %w", err)
	}
	return &RelayNftEvents{r}, nil
}

func (r *RelayNftEvents) SaveEvents(events []SaveEventRequest) error {
	_, err := relay.Operation(r.URL, nodeId, "nft_events.save_events", gtv.SerializableFromSlice(events))
	if err != nil {
		return fmt.Errorf("[RelayNftEvents][SaveEvents] Failed to post events %w", err)
	}

	return nil
}
