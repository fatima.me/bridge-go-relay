package nftevents

import (
	"fmt"
	"github.com/ethereum/go-ethereum/core/types"
	"bitbucket.org/chromawallet/go-relay/go/utils"
)

func LogsToSaveEventRequest(logs []*types.Log, chain string) ([]SaveEventRequest, error) {
	var events = make([]SaveEventRequest, len(logs))
	for i, log := range logs {
		t, err := utils.TransferArgsFromLog(log)
		if err != nil {
			return nil, err
		}

		events[i] = SaveEventRequest{
			Chain:     chain,
			Contract:  log.Address.Hex(),
			NftId:     fmt.Sprintf("%d", t.TokenId),
			ItemType:  t.ItemType,
			Tx:        log.TxHash.Hex(),
			EventType: EventTypeTransfer,
			EventId:   0,
			Details: TransferDetailsDto{
				From:   t.From,
				To:     t.To,
				Amount: t.Amount,
			},
			Removed: log.Removed,
		}
	}
	return events, nil
}
