package nftevents

import (
	"bitbucket.org/chromawallet/go-relay/go/gtv"
	"bitbucket.org/chromawallet/go-relay/go/utils"
)

type (
	SaveEventRequest struct {
		Chain     string           `json:"chain"`
		EventId   int              `json:"eventId"`
		Contract  string           `json:"contract"`
		NftId     string           `json:"nftId"`
		ItemType  utils.ItemType   `json:"itemType"`
		Tx        string           `json:"tx"`
		EventType EventType        `json:"eventType"`
		Removed   bool             `json:"removed"`
		Details   gtv.Serializable `json:"details"`
	}

	SaleDetailsDto struct {
		From           string         `json:"from"`
		To             string         `json:"to"`
		PayedToken     string         `json:"payedToken"`
		PayedItemType  utils.ItemType `json:"payedItemType"`
		PayedAmount    string         `json:"payedAmount"`
		ReceivedAmount string         `json:"receivedAmount"`
		SaleType       SaleType       `json:"saleType"`
	}

	TransferDetailsDto struct {
		From   string
		To     string
		Amount string
	}
)

func (s SaveEventRequest) Gtv() interface{} {
	return []interface{}{s.Chain, s.EventId, s.Contract, s.NftId, s.ItemType, s.Tx, s.EventType, s.Removed, s.Details.Gtv()}
}

func (s SaleDetailsDto) Gtv() interface{} {
	return []interface{}{s.From, s.To, s.PayedToken, s.PayedItemType, s.PayedAmount, s.ReceivedAmount, s.SaleType}
}

func (t TransferDetailsDto) Gtv() interface{} {
	return []interface{}{t.From, t.To, t.Amount}
}
