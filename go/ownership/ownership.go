package ownership

import (
	"bitbucket.org/chromawallet/go-relay/go/gtv"
	"fmt"
	"bitbucket.org/chromawallet/go-relay/go/relay"
	"go.uber.org/zap"
)

const (
	NodeId = "ownership"

	OpAddContract        = "bridge.add_contract"
	OpAddContracts       = "bridge.add_contracts"
	OpTransferOwnerships = "bridge.transfer_ownerships"
	OpChangeSyncStates   = "bridge.change_sync_states"
	QueryGetContract     = "bridge.get_contract"
	QueryGetContracts    = "bridge.get_contracts"
)

type RelayOwnership struct {
	*relay.Relay
}

func NewRelay(relayUrl string, logger *zap.Logger) (*RelayOwnership, error) {
	r, err := relay.NewRelay(relayUrl, logger)
	if err != nil {
		return nil, err
	}
	return &RelayOwnership{
		r,
	}, nil
}

func (o *RelayOwnership) TransferOwnerships(chain string, maxBlock uint64, contractTransfersRequests []ContractTransfersRequest) error {
	_, err := relay.Operation(o.URL, NodeId, OpTransferOwnerships,
		gtv.String(chain),
		gtv.Uint64(maxBlock),
		gtv.SerializableFromSlice(contractTransfersRequests),
	)
	if err != nil {
		return fmt.Errorf("operation failed %s: %w", OpTransferOwnerships, err)
	}

	return err
}

func (o *RelayOwnership) AddContract(contract ContractInfo) error {
	addContractRequest, err := contractInfoToAddContractRequest(contract)
	if err != nil {
		return err
	}
	_, err = relay.Operation(o.URL, NodeId, OpAddContract,
		gtv.String(contract.Chain),
		addContractRequest,
	)
	if err != nil {
		return fmt.Errorf("operation failed %s: %w", OpAddContract, err)
	}
	return nil
}

func (o *RelayOwnership) AddContracts(chain string, contracts []ContractInfo) error {
	var requests []AddContractRequest
	for _, contract := range contracts {
		addContractRequest, err := contractInfoToAddContractRequest(contract)
		if err != nil {
			return err
		}

		requests = append(requests, addContractRequest)
	}

	_, err := relay.Operation(o.URL, NodeId, OpAddContracts,
		gtv.String(chain),
		gtv.SerializableFromSlice(requests),
	)
	if err != nil {
		return fmt.Errorf("operation failed %s: %w", OpAddContracts, err)
	}

	return nil
}

func (o *RelayOwnership) ChangeSyncStates(chain string, addresses []string, state SyncState) error {
	_, err := relay.Operation(o.URL, NodeId, OpChangeSyncStates,
		gtv.String(chain),
		gtv.StringSlice(addresses),
		gtv.Int(state),
	)
	if err != nil {
		return fmt.Errorf("operation failed %s: %w", OpChangeSyncStates, err)
	}
	return nil
}

func (o *RelayOwnership) GetContract(g ContractGet) (ContractInfo, error) {
	contractInfo := ContractInfo{}
	_, err := relay.Query(o.URL, NodeId, QueryGetContract, g, &contractInfo)
	if err != nil {
		return contractInfo, err
	}
	return contractInfo, err
}

func (o *RelayOwnership) GetContracts() ([]ContractInfo, error) {
	var contractInfos []ContractInfo
	_, err := relay.Query(o.URL, NodeId, QueryGetContracts, nil, &contractInfos)
	if err != nil {
		return contractInfos, err
	}
	return contractInfos, err
}

func contractInfoToAddContractRequest(contract ContractInfo) (AddContractRequest, error) {
	tokenType, err := TokenTypeFromString(contract.Type)
	if err != nil {
		return AddContractRequest{}, err
	}
	return AddContractRequest{
		Address:       contract.Address,
		TokenType:     tokenType,
		CreationBlock: contract.LastBlockNumber,
	}, nil
}
