package ownership

import (
	"bitbucket.org/chromawallet/go-relay/go/gtv"
	"errors"
	"strings"
)

type (
	TransferRequest struct {
		TokenId     int64
		OwnerId     string
		BlockNumber int64
		TxHash      string
		Removed     bool
	}

	ContractTransfersRequest struct {
		Contract  string
		Transfers []TransferRequest
	}

	AddContractRequest struct {
		Address       string
		TokenType     TokenType
		CreationBlock int64
	}

	SyncStatePost struct {
		Chain     string
		Addresses []string
		State     SyncState
	}

	TokenType int
)

const (
	TokenTypeErc721 TokenType = iota
	TokenTypeErc1155
)

func (t TransferRequest) Gtv() interface{} {
	return []interface{}{t.TokenId, t.OwnerId, t.BlockNumber, t.TxHash, t.Removed}
}

func (c ContractTransfersRequest) Gtv() interface{} {
	return []interface{}{c.Contract, gtv.SerializableFromSlice(c.Transfers).Gtv()}
}

func (s SyncStatePost) Gtv() interface{} {
	return []interface{}{s.Chain, gtv.BytesSliceFromHexSlice(s.Addresses), s.State}
}

func (a AddContractRequest) Gtv() interface{} {
	return []interface{}{a.Address, gtv.Int(a.TokenType), gtv.Int64(a.CreationBlock)}
}

type ContractGet struct {
	Address gtv.Bytes `json:"address"`
	Chain   string    `json:"chain"`
}

type ContractInfo struct {
	Address         string `json:"address,omitempty"`
	Chain           string `json:"chain,omitempty"`
	Type            string `json:"type,omitempty"`
	LastBlockNumber int64  `json:"lastBlockNumber,omitempty"`
}

func TokenTypeFromString(value string) (TokenType, error) {
	switch strings.ToLower(value) {
	case "erc721":
		return TokenTypeErc721, nil
	case "erc1155":
		return TokenTypeErc1155, nil
	}
	return TokenTypeErc721, errors.New("token type " + value + " is not supported")
}
