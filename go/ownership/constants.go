package ownership

type SyncState int

const (
	SyncStateSynced SyncState = iota
	SyncStateUnsynced
)

func (s SyncState) String() string {
	switch s {
	case SyncStateSynced:
		return "Synced"
	case SyncStateUnsynced:
		return "Unsynced"
	}
	return "Unknown"
}
