package ownership

import (
	"github.com/ethereum/go-ethereum/core/types"
	"bitbucket.org/chromawallet/go-relay/go/utils"
)

func LogsToTransferRequest(logs []*types.Log) ([]TransferRequest, error) {
	var transfers = make([]TransferRequest, len(logs))
	for i, log := range logs {
		t, err := utils.TransferArgsFromLog(log)
		if err != nil {
			return nil, err
		}

		transfers[i] = TransferRequest{
			TokenId:     t.TokenId,
			OwnerId:     t.To,
			BlockNumber: int64(log.BlockNumber),
			TxHash:      log.TxHash.Hex(),
			Removed:     log.Removed,
		}
	}
	return transfers, nil
}
