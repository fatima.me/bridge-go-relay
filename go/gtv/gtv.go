package gtv

type Serializable interface {
	Gtv() interface{}
}

type SliceGtvSerializable []Serializable

func (s SliceGtvSerializable) Gtv() interface{} {
	gtvs := make([]interface{}, len(s))
	for i, elem := range s {
		gtvs[i] = elem.Gtv()
	}
	return gtvs
}

func SerializableFromSlice[T Serializable](slice []T) SliceGtvSerializable {
	sgs := make(SliceGtvSerializable, len(slice))
	for i, e := range slice {
		v, _ := interface{}(e).(Serializable)
		sgs[i] = v
	}
	return sgs
}
