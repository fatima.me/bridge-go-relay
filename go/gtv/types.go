package gtv

import "strings"

type String string
type StringSlice []string
type Int int
type Int64 int
type Uint64 uint64
type Bytes string
type BytesSlice []Bytes

func (s String) Gtv() interface{} {
	return s
}

func (s StringSlice) Gtv() interface{} {
	return s
}

func (i Int) Gtv() interface{} {
	return i
}

func (i Int64) Gtv() interface{} {
	return i
}

func (u Uint64) Gtv() interface{} {
	return u
}

func (b Bytes) Gtv() interface{} {
	return b
}

func (b BytesSlice) Gtv() interface{} {
	return b
}

func BytesFromHex(hex string) Bytes {
	b := strings.TrimPrefix(hex, "0x")
	return Bytes(b)
}

func BytesSliceFromHexSlice(hexes []string) BytesSlice {
	bs := make([]Bytes, len(hexes))
	for i, hex := range hexes {
		bs[i] = Bytes(strings.TrimPrefix(hex, "0x"))
	}

	return bs
}
